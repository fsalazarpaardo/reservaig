﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AplicacionWeb.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:textbox ID="txtSaludos" runat="server" ValidationGroup="valSaludo"></asp:textbox>
            <asp:RequiredFieldValidator ID="rfvTxtSaludo" runat="server" ErrorMessage="El campo saludo es requerido" ControlToValidate="txtSaludos" ValidationGroup="valSaludo" ForeColor="Red">*</asp:RequiredFieldValidator>
            <asp:CustomValidator ID="cvSaludo" runat="server" ErrorMessage="No es nobre valido" ControlToValidate="txtSaludos" OnServerValidate="CustomValidator1_ServerValidate">*</asp:CustomValidator>
            <asp:Button ID="btnSaludos" runat="server" Text="Saludo" OnClick="btnSaludos_Click" ValidationGroup="valSaludo" />
            <br />
            <asp:ValidationSummary ID="vsSaludo" runat="server" ForeColor="Red" ValidationGroup="valSaludo" />
            <asp:Label ID="lblSaludos" runat="server" Text=""></asp:Label>
        </div>
        <asp:GridView ID="gvListaUsuarios" runat="server" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
            <AlternatingRowStyle BackColor="White" />
            <FooterStyle BackColor="#CCCC99" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#F7F7DE" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FBFBF2" />
            <SortedAscendingHeaderStyle BackColor="#848384" />
            <SortedDescendingCellStyle BackColor="#EAEAD3" />
            <SortedDescendingHeaderStyle BackColor="#575357" />
        </asp:GridView>
    </form>
</body>

</html>
