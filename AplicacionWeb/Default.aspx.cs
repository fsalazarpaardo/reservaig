﻿using ReservaIG.Modelo;
using ReservaIG.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AplicacionWeb
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtSaludos.Text = "Pepito Perez";
                List<Usuario> usuarios = new ModeloFachada().ListarUsuarios();
                gvListaUsuarios.DataSource = usuarios;
                gvListaUsuarios.DataBind();
            }
            else
            {
                lblSaludos.Text = "Hola " + txtSaludos.Text;
            }
        }

        protected void btnSaludos_Click(object sender, EventArgs e)
        {
            if (txtSaludos.Text.Equals(""))
            {
                rfvTxtSaludo.IsValid = false;
                txtSaludos.Text = "";
            }
            
        }

        protected void odsGridUsuarios_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
                
        }
    }
}