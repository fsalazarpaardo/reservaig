﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Entidades
{
    [Serializable]
   public class Tipo
    {
        public int TipoId { get; set; }
        public int Descripcion { get; set; }
    }
}
