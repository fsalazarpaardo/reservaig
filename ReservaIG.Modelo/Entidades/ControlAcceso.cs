﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Entidades
{
    class ControlAcceso
    {
        public int AsegurableId { get; set; }
        public int RolId { get; set; }
    }
}
