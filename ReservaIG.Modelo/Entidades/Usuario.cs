﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Entidades
{
    public class Usuario
    {
        public int UsuarioId { get; set; }
        public string UsuarioLogin { get; set; }
        public string UsuarioPassword { get; set; }
        public string UsuarioEstado { get; set; }
        public string UsuarioCrea { get; set; }
        public byte[] FotoPerfil { get; set; }

    }
}
