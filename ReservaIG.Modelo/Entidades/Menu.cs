﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Entidades
{
    public class Menu
    {
        public int AsegurableId { get; set; }
        public string Nombre { get; set; }
        public string Ruta { get; set; }
        public int? ParentId { get; set; }
        public int TipoAsegurableId { get; set; }

    }
}
