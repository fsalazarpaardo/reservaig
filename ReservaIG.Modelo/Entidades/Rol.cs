﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Entidades
{
   public class Rol
    {
        public int RolId { get; set; }
        public string Nombre { get; set; }
    }
}
