﻿using ReservaIG.Modelo.Datos;
using ReservaIG.Modelo.Entidades;
using ReservaIG.Modelo.Utilidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Logica
{
    class RolBL
    {
        internal List<Rol> ListarRoles()
        {
            DataTable dtRol = new Dao().EjecutarStoredProcedureTD(Constantes.SP_OBTENER_ROLES);

            List<Rol> rol = new List<Rol>();
            Rol item;
            foreach (DataRow fila in dtRol.Rows)
            {
                item = new Rol();
                item.RolId = int.Parse(fila["id"].ToString());
                item.Nombre = fila["descripcion"].ToString();
                rol.Add(item);
            }
            return rol;
        }
        internal bool CrearRol(Rol rol)
        {
            try
            {
                Dictionary<string, object> datosRol = new Dictionary<string, object>();
                datosRol.Add("nombreRol", rol.Nombre);
                new Dao().EjecutarStoredProcedure(Constantes.SP_CREAR_ROL, datosRol);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
