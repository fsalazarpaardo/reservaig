﻿using ReservaIG.Modelo.Datos;
using ReservaIG.Modelo.Entidades;
using ReservaIG.Modelo.Utilidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Logica
{
    internal class UsuarioBL
    {
        internal bool CrearUsuario(Usuario objUsuario, int rolId)
        {
            try
            {
                Dictionary<string, object> datosUsuarios = new Dictionary<string, object>();

                datosUsuarios.Add("usuario", objUsuario.UsuarioLogin);
                datosUsuarios.Add("password", new SecurityHelper().getHash(objUsuario.UsuarioPassword));
                datosUsuarios.Add("usuarioCrea", objUsuario.UsuarioCrea);
                datosUsuarios.Add("rolId", rolId);
                
                new Dao().EjecutarStoredProcedure(Constantes.SP_CREAR_USUARIO, datosUsuarios);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal List<Usuario> ListarUSuarios()
        {
            string sqlString = "SELECT u.[id], u.[loguin], u.[password], eu.nombre FROM[Seguridad].[Usuarios] u inner join[Seguridad].[EstadoUsuario] eu on eu.id = u.estadoUsuarioId";
            DataTable dtUsuarios = new Dao().GetDataTable(sqlString);
            List<Usuario> listaUsuarios = new List<Usuario>();
            Usuario usuario;
            foreach (DataRow fila in dtUsuarios.Rows)
            {
                usuario = new Usuario();
                usuario.UsuarioId = int.Parse(fila["id"].ToString());
                usuario.UsuarioLogin = fila["loguin"].ToString();
                usuario.UsuarioPassword = fila["password"].ToString();
                usuario.UsuarioEstado = fila["estadoUsuarioId"].ToString();
                listaUsuarios.Add(usuario);
            }
            return listaUsuarios;
        }
        internal bool ValidarNombreUsuario(string username)
        {
            string sqlString = @"SELECT [id] FROM [Seguridad].[Usuarios] WHERE [Seguridad].[Usuarios].[loguin]='" + username + "'";

            DataTable dtUsuarios = new Dao().GetDataTable(sqlString);

            if (dtUsuarios.Rows.Count > 0)
                return false;
            else
                return true;
        }

        internal Usuario ObtenerUsuario(string username)
        {
            string sqlString = @"SELECT [UsuarioId],[Username],[Password],[Estado], [FotoPerfil] FROM[security].[Usuarios] WHERE username = '" + username + "'";

            DataTable dtUsuarios = new Dao().GetDataTable(sqlString);
            Usuario usuario;
            usuario = new Usuario();
            if (dtUsuarios.Rows.Count > 0)
            {
                DataRow fila = dtUsuarios.Rows[0];

                usuario.UsuarioId = int.Parse(fila["UsuarioId"].ToString());
                usuario.UsuarioLogin = fila[1].ToString();
                usuario.UsuarioPassword = fila["Password"].ToString();
                usuario.UsuarioEstado = fila["Estado"].ToString();
                usuario.FotoPerfil = (byte[])fila["FotoPerfil"];

            }
            return usuario;
        }

    }

}
