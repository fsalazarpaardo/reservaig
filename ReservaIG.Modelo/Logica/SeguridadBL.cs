﻿using ReservaIG.Modelo.Datos;
using ReservaIG.Modelo.Entidades;
using ReservaIG.Modelo.Utilidades;
using System.Collections.Generic;
using System.Data;

namespace ReservaIG.Modelo.Logica
{
    internal class SeguridadBL
    {
        internal bool Autenticar(string username, string password)
        {
            string hash = new Utilidades.SecurityHelper().getHash(password);
            Dictionary<string, object> datosUsuarios = new Dictionary<string, object>();
            datosUsuarios.Add("username", username);
            datosUsuarios.Add("password", hash);
            DataTable dataTable = new Dao().EjecutarStoredProcedureTD(Constantes.SP_LOGUIN, datosUsuarios);
            if (dataTable.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        internal List<Menu> obtenerMenu(string username)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("ps_username", username);
            DataTable dtMenu = new Dao().EjecutarStoredProcedureTD(Constantes.SP_OBTENER_MENU, parametros);

            List<Menu> menu = new List<Menu>();
            Menu item;
            foreach (DataRow fila in dtMenu.Rows)
            {
                item = new Menu();
                item.AsegurableId = int.Parse(fila["id"].ToString());
                item.Nombre = fila["Nombre"].ToString();
                if (string.IsNullOrWhiteSpace(fila["padreId"].ToString()))
                {
                    item.ParentId = null;
                }
                else
                {
                    item.ParentId = int.Parse(fila["padreId"].ToString());
                }
                item.Ruta = fila["Ruta"].ToString();

                menu.Add(item);
            }

            return menu;
        }
        internal List<Menu> obtenerMenu()
        {
            DataTable dtMenu = new Dao().EjecutarStoredProcedureTD(Constantes.SP_OBTENER_LISTAMENU);

            List<Menu> menu = new List<Menu>();
            Menu item;
            foreach (DataRow fila in dtMenu.Rows)
            {
                item = new Menu();
                item.AsegurableId = int.Parse(fila["id"].ToString());
                item.Nombre = fila["Nombre"].ToString();
                if (string.IsNullOrWhiteSpace(fila["padreId"].ToString()))
                {
                    item.ParentId = null;
                }
                else
                {
                    item.ParentId = int.Parse(fila["padreId"].ToString());
                }
                item.Ruta = fila["Ruta"].ToString();

                menu.Add(item);
            }

            return menu;
        }

    }
}
