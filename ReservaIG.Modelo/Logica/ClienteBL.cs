﻿using ReservaIG.Modelo.Datos;
using ReservaIG.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Logica
{
    internal class ClienteBL
    {
        public Cliente GetCliente(string cedula)
        {
            string sqlString = @"SELECT [id],[nombre],[telefono] FROM [ReservasIG].[Cliente] WHERE id = '" + cedula + "'";

            DataTable dtCliente = new Dao().GetDataTable(sqlString);
            Cliente cliente;
            cliente = new Cliente();
            if (dtCliente.Rows.Count > 0)
            {
                DataRow fila = dtCliente.Rows[0];

                cliente.ClienteId = int.Parse(fila["id"].ToString());
                cliente.Nombre = fila["nombre"].ToString();
                cliente.Telefono = fila["telefono"].ToString();

            }
            return cliente;
        }

        internal List<Cliente> GetClientes()
        {
            string sqlString = @"SELECT [id],[nombre],[telefono] FROM [ReservasIG].[Cliente] ";

            DataTable dtCliente = new Dao().GetDataTable(sqlString);
            List<Cliente> lstClientes = new List<Cliente>();
            Cliente cliente;

            if (dtCliente.Rows.Count > 0)
            {
                foreach (DataRow fila in dtCliente.Rows)
                {
                    cliente = new Cliente();
                    cliente.ClienteId = int.Parse(fila["ClienteId"].ToString());
                    cliente.Nombre = fila["nombre"].ToString();
                    cliente.Telefono = fila["telefono"].ToString();
                    lstClientes.Add(cliente);
                }

            }
            return lstClientes;
        }

    }
}
