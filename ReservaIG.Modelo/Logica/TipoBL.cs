﻿using ReservaIG.Modelo.Datos;
using ReservaIG.Modelo.Entidades;
using ReservaIG.Modelo.Utilidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Logica
{
    class TipoBL
    {
        internal bool AgregarTiposBatch(List<Tipo> lstTipos)
        {
            bool resp = true;
            DataTable tblTipos = new DataTable();
            try
            {
                string query = @"SELECT top 0 TipoId, Descrpcion FROM dbo.Tipos";
                tblTipos = new Dao().GetDataTable(query);

                foreach (var tipo in lstTipos)
                {
                    DataRow oRow = tblTipos.NewRow();
                    oRow["TipoId"] = tipo.TipoId;
                    oRow["Descrpcion"] = tipo.Descripcion;
                    tblTipos.Rows.Add(oRow);
                }

                resp = new Dao().UpdateDataTable(tblTipos, query);
            }
            catch (Exception e)
            {
                new LogHelper().EscribirLog(LogType.error, (e.InnerException ?? e).Message);
                resp = false;
            }
            return resp;
        }

        internal bool AgregarTiposXml(List<Tipo> lstTipos)
        {
            string xml = SerializerHelper.Serialize(lstTipos);
            bool resp = false;

            try
            {
                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("XmlTipo", xml);

                new Dao().EjecutarStoredProcedure(Constantes.spInsertXml, parametros);
                resp = true;
            }
            catch (Exception ex)
            {
                new LogHelper().EscribirLog(LogType.error, (ex.InnerException ?? ex).Message);
                resp = false;
            }

            return resp;
        }

    }
}
