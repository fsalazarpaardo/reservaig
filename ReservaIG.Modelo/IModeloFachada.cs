﻿using ReservaIG.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo
{
    interface IModeloFachada
    {
        bool CrearUsuario(Usuario usuario, int RolId);
        List<Rol> ListarRoles();
        List<Usuario> ListarUsuarios();
        bool ValidarNombreUsuario(string username);
        Usuario ObtenerUsuario(string username);
        bool AgregarTiposBatch(List<Tipo> lstTipos);
        bool AgregarTiposXml(List<Tipo> lstTipos);
        Cliente GetCliente(string cedula);
        List<Cliente> GetClientes();
        bool CrearRol(Rol rol);



    }
}
