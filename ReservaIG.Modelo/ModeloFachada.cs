﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReservaIG.Modelo.Entidades;
using ReservaIG.Modelo.Logica;

namespace ReservaIG.Modelo
{
    //[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ModeloFachada : IModeloFachada
    {
       
        
        public bool CrearUsuario(Usuario objUsuario, int RolId)
        {
            return new UsuarioBL().CrearUsuario(objUsuario, RolId);
        }

        public List<Rol> ListarRoles()
        {
            return new RolBL().ListarRoles();
        }

        public List<Usuario> ListarUsuarios()
        {
            return new UsuarioBL().ListarUSuarios();
        }


        public bool ValidarNombreUsuario(string username)
        {
            return new UsuarioBL().ValidarNombreUsuario(username);
        }

        public Usuario ObtenerUsuario(string username)
        {
            return new UsuarioBL().ObtenerUsuario(username);
        }

        public bool AgregarTiposBatch(List<Tipo> lstTipos)
        {
            throw new NotImplementedException();
        }

        public bool AgregarTiposXml(List<Tipo> lstTipos)
        {
            throw new NotImplementedException();
        }

        public Cliente GetCliente(string userName)
        {
            return new ClienteBL().GetCliente(userName);
        }

        public List<Cliente> GetClientes()
        {
            return new ClienteBL().GetClientes();
        }

        public bool CrearRol(Rol rol)
        {
            return new RolBL().CrearRol(rol);
        }
    }
}
