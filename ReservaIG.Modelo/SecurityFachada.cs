﻿using ReservaIG.Modelo.Logica;
using ReservaIG.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ReservaIG.Modelo
{
   public class SecurityFachada
    {
        public bool AutenticarUsuario(string username, string password)
        {
            return new SeguridadBL().Autenticar(username, password);
        }
        public List<Menu> ObtenerMenu(string username)
        {
            return new SeguridadBL().obtenerMenu(username);
        }
        public List<Menu> ObtenerMenu()
        {
            return new SeguridadBL().obtenerMenu();
        }
        

    }
}
