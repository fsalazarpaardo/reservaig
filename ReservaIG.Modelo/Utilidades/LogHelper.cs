﻿using ReservaIG.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Utilidades
{
    class LogHelper
    {
        private string path = ConfigurationManager.AppSettings["RutaLog"].ToString();

        public void EscribirLog(LogType type, string mensaje)
        {
            string nombre = "Log-" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + ".txt";
            string logMessage = DateTime.Now.ToString() + " ";
            logMessage = logMessage + type.ToString() + " ";
            logMessage = logMessage + mensaje;
            try
            {
                using (StreamWriter sw = new StreamWriter(path + nombre, true))
                {
                    sw.WriteLine(logMessage);
                    sw.Close();
                }
            }
            catch { }
        }

        public string LeerLog()
        {
            string nombre = "Log-" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + ".txt";
            StringBuilder builder = new StringBuilder();
            string s;
            using (StreamReader sr = new StreamReader(path + nombre))
            {
                s = "";
                while ((s = sr.ReadLine()) != null)
                    builder.AppendLine(s);
            }

            return builder.ToString();
        }

        public void EscribirLogXml(LogType type, string mensaje)
        {
            string nombre = "LogXml-" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + ".xml";
            LogDto objLog = new LogDto();

            objLog.FechaLog = DateTime.Now;
            objLog.TipoLog = type.ToString();
            objLog.Mensaje = mensaje;

            string logMessage = SerializerHelper.Serialize<LogDto>(objLog);
            try
            {
                using (StreamWriter sw = new StreamWriter(path + nombre, true))
                {
                    sw.WriteLine(logMessage);
                    sw.Close();
                }
            }
            catch { }

        }

        public List<LogDto> LeerLogXml()
        {
            string nombre = "LogXml-" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + ".xml";
            List<LogDto> lstLogDtos = new List<LogDto>();
            LogDto logDto;

            string s;
            using (StreamReader sr = new StreamReader(path + nombre))
            {
                s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    logDto = new LogDto();
                    logDto = SerializerHelper.Deserialize<LogDto>(s);
                    lstLogDtos.Add(logDto);
                }
            }

            return lstLogDtos;
        }

    }
    public enum LogType
    {
        info = 1,
        advertencia = 2,
        error = 3,
        fatal = 4
    }
    
}
