﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservaIG.Modelo.Utilidades
{
    internal static class Constantes        
    {
        internal const string SP_LOGUIN = "Seguridad.spAutenticar";
        internal const string SP_CREAR_USUARIO = "Seguridad.spCrearUsuario";
        internal const string SP_OBTENER_MENU = "Seguridad.obtenerMenu";
        internal const string SP_OBTENER_ROLES = "Seguridad.ObtenerRoles";
        internal const string SP_OBTENER_LISTAMENU = "Seguridad.ObtenerListaMenu";
        internal const string SP_CREAR_CONTROL_ACCESO = "Seguridad.spCrearControlAcceso";
        internal const string spInsertXml = "Seguridad.InsertarXmlTipo";
        internal const string SP_CREAR_ROL = "Seguridad.spCrearRol";
    }
}
