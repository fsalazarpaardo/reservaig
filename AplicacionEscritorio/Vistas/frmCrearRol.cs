﻿using AplicacionEscritorio.Utils;
using ReservaIG.Modelo;
using ReservaIG.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AplicacionEscritorio.Vistas
{
    public partial class frmCrearRol : Form
    {
        public frmCrearRol()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardarRol_Click(object sender, EventArgs e)
        {
            string nombreRol  = txtNombreRol.Text;
            if (string.IsNullOrWhiteSpace(nombreRol))
            {
                MessageBox.Show("El nombre del Rol es requerido.",
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1);
                return;
            }
            Rol rol = new Rol();
            rol.Nombre = nombreRol;
            ModeloFachada fachada = new ModeloFachada();
            fachada.CrearRol(rol);
            DialogResult result = MessageBox.Show("Se creo el Rol correctamente.\r\n¿Desea continuar?",
                   "Info",
                   MessageBoxButtons.YesNo,
                   MessageBoxIcon.Information,
                   MessageBoxDefaultButton.Button1);
            if (result.ToString() == "Yes")
            {
                Utilidades.LimpiarFormulario(this.Controls);
            }
            else
            {
                this.Close();
            }
            
        }

    }
}
