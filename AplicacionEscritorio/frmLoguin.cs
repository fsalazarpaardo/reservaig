﻿using System;
using ReservaIG.Modelo;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AplicacionEscritorio.Utils;

namespace AplicacionEscritorio
{
    public partial class frmLoguin : Form
    {
        public frmLoguin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                string nombreUsuario = txtNombreUsuario.Text;
                string contrasena = txtContrasena.Text;

                SecurityFachada security = new SecurityFachada();
                bool validar = security.AutenticarUsuario(nombreUsuario, contrasena);
                if (validar)
                {
                    SessionHelper.username = nombreUsuario;
                    this.Hide();
                    Principal frmPrincipal = new Principal();
                    frmPrincipal.Show();
                }
                else
                {
                    MessageBox.Show("Usuario y/o Contraseña incorrecto."
                        , "Error"
                        , MessageBoxButtons.OK
                        ,MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message
                         , "Error"
                         , MessageBoxButtons.OK
                         , MessageBoxIcon.Error,
                         MessageBoxDefaultButton.Button1);
            }


        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
