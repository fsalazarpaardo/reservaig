﻿using ReservaIG.Modelo;
using AplicacionEscritorio.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace AplicacionEscritorio
{
    public partial class Principal : Form
    {
        private int childFormNumber = 0;
        List<ReservaIG.Modelo.Entidades.Menu> menus;

        public Principal()
        {
            InitializeComponent();
            menus = new SecurityFachada().ObtenerMenu(SessionHelper.username);
            List<ReservaIG.Modelo.Entidades.Menu> menuPadres = menus.Where(p => p.ParentId == null).ToList();
            CrearMenu(menuPadres);
        }

        private void CrearMenu(List<ReservaIG.Modelo.Entidades.Menu> menus)
        {
            foreach (var item in menus)
            {
                ToolStripMenuItem menuItem = new ToolStripMenuItem(item.Nombre);
                List<ReservaIG.Modelo.Entidades.Menu> menuHijos = this.menus.Where(p => p.ParentId == item.AsegurableId).ToList();
                CrearMenu(menuItem, menuHijos);
                menuPrincipal.Items.Add(menuItem);
            }

        }

        private void CrearSubMenu()
        {

        }


        private void CrearMenu(ToolStripMenuItem menuItem, List<ReservaIG.Modelo.Entidades.Menu> menus)
        {
            foreach (var item in menus)
            {
                ToolStripMenuItem menuItemHijo = new ToolStripMenuItem(item.Nombre);
                menuItemHijo.Click += MenuItemHijo_Click;
                menuItemHijo.Tag = item;
                menuItem.DropDownItems.Add(menuItemHijo);
            }

        }

        private void MenuItemHijo_Click(object sender, EventArgs e)
        {
           Assembly frmAssembly = Assembly.LoadFile(Application.ExecutablePath);

            foreach (Type type in frmAssembly.GetTypes())

            {
                if (type.BaseType == typeof(Form))
                {
                    var nombreMenu = NombreMenu(sender);
                    if (string.IsNullOrWhiteSpace(nombreMenu))
                    {
                        this.Close();
                    }
                    if (type.Name == nombreMenu)
                    {
                        if (!formularioAbierto(type.Name))
                        {
                            Form frmShow = (Form)frmAssembly.CreateInstance(type.ToString());

                            frmShow.MdiParent = this;
                            frmShow.Show();
                        }
                    }
                }

            }

        }

        private bool formularioAbierto(string nombreFormulario)
        {
            foreach (Form frm in Application.OpenForms)
            {
                if(frm.Name == nombreFormulario)
                {
                    return true;
                }
            }
            return false;
        }

        private string NombreMenu(object sender)
        {
            var nombreMenu = "";
            if (sender is ToolStripMenuItem)
            {
                var menuOption = sender as ToolStripMenuItem;
                if (menuOption.Tag != null)
                {
                    if (menuOption.Tag is ReservaIG.Modelo.Entidades.Menu)
                    {

                        var menur = menuOption.Tag as ReservaIG.Modelo.Entidades.Menu;
                        nombreMenu = menur.Ruta;
                    }
                }

            }
            return nombreMenu;
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //sstatusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }
    }
}
