﻿using ReservaIG.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AplicacionEscritorio.Utilidades
{
    internal class Validaciones
    {
        /// <summary>
        /// Validar si el password cumple con la politica de:
        /// Minino 8 caracteres.
        /// Minimo una minuscula.
        /// Minimo una mayuscula.
        /// Minimo un caractere especial
        /// </summary>
        /// <param name="password"></param>
        /// <returns>bool</returns>
        internal bool ValidarPassword(string password)
        {
            Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$");
            return regex.IsMatch(password);
        }
        /// <summary>
        /// Valida si un usuario existe en una base de datos
        /// </summary>
        /// <param name="username"></param>
        /// <returns>bool</returns>
        internal bool ValidarUsarName(string username)
        {
            return new ModeloFachada().ValidarNombreUsuario(username);
        }
    }
}
