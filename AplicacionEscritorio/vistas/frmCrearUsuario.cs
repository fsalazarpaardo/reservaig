﻿using System;
using ReservaIG.Modelo;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AplicacionEscritorio.Vistas;
using AplicacionEscritorio.Utils;
using ReservaIG.Modelo.Entidades;
using AplicacionEscritorio.ControlUsuarios;

namespace AplicacionEscritorio.Vistas
{
    public partial class frmCrearUsuario : Form
    {
        List<ReservaIG.Modelo.Entidades.Menu> menus;
        public frmCrearUsuario()
        {
            InitializeComponent();

            List<ReservaIG.Modelo.Entidades.Menu> menus = this.ObtenerMenu();
            List<Rol> rol = new ModeloFachada().ListarRoles();
            LlenarComboBox(rol, cbRol);
        }
        
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario())
            {
                Usuario usuario = new Usuario();
                usuario.UsuarioLogin = txtNombreUsuario.Text;
                usuario.UsuarioPassword = txtContrasena.Text;
                usuario.UsuarioCrea = SessionHelper.username;
                ComboboxItem comboboxItem = (ComboboxItem) cbRol.SelectedItem;
                ModeloFachada fachada = new ModeloFachada();
                fachada.CrearUsuario(usuario, comboboxItem.Value);
                Utilidades.LimpiarFormulario(this.Controls);
                MessageBox.Show("Usuario creado correctamente.",
                    "Info",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1);
            }

        }
        private bool ValidarFormulario()
        {
            string username = txtNombreUsuario.Text.Trim();
            string password = txtContrasena.Text.Trim();
            string rolItems = cbRol.SelectedItem.ToString();
            string mensaje = string.Empty;
            bool resultado = true;
            if (string.IsNullOrWhiteSpace(username))
            {
                mensaje += "El nombre de Usuario es requerido.\r\n";
                resultado = false;
            }
            if (string.IsNullOrWhiteSpace(password))
            {
                mensaje += "La contraseña es requerida.\r\n";
                resultado = false;
            }
            if (rolItems.Equals("Seleccione"))
            {
                mensaje += "El Rol es requerido.";
                resultado = false;
            }
            if (!resultado)
            {
                MessageBox.Show(mensaje,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button1);
                return resultado;
            }
            
           /* if (Validaciones.ValidarPassword(password))
            {
                MessageBox.Show("El password debe contener 8 caracteres, una mayúscula, 1 minúscula y un carácter especial.");
                return false;
            }*/
            if (!Validaciones.ValidarUsername(username))
            {
                MessageBox.Show("El usuario ya existe.");
                return false;
            }
            return resultado;
        }

        private List<ReservaIG.Modelo.Entidades.Menu> ObtenerMenu()
        {
            this.menus = new SecurityFachada().ObtenerMenu();
            List<ReservaIG.Modelo.Entidades.Menu> menuPadres = this.menus.Where(p => p.ParentId == null).ToList();
            return menuPadres;
        }

        private List<ReservaIG.Modelo.Entidades.Menu> ObtenerMenu(string username)
        {
            this.menus = new SecurityFachada().ObtenerMenu(username);
            List<ReservaIG.Modelo.Entidades.Menu> menuPadres = this.menus.Where(p => p.ParentId == null).ToList();
            return menuPadres;
        }

        private List<ReservaIG.Modelo.Entidades.Menu> ObtenerMenu(ReservaIG.Modelo.Entidades.Menu menu)
        {
            List<ReservaIG.Modelo.Entidades.Menu> menuHijos = this.menus.Where(p => p.ParentId == menu.AsegurableId).ToList();
            return menuHijos;
        }

        private void LlenarComboBox(List<Rol> rol, ComboBox comboBox)
        {
            comboBox.Items.Clear();
           comboBox.Items.Add("Seleccione");
            foreach (var item in rol)
            {
                ComboboxItem comboboxItem = new ComboboxItem();
                comboboxItem.Value = item.RolId;
                comboboxItem.Text = item.Nombre;
                comboBox.Items.Add(comboboxItem);
            }
            comboBox.SelectedIndex = 0;
        }

        private void LlenarCheckListBox(List<ReservaIG.Modelo.Entidades.Menu> menus, CheckedListBox checkedListBox)
        {
            foreach (var item in menus)
            {
                removerItems(item, checkedListBox);
                checkedListBox.Items.Add(item.Nombre);
            }
        }

        private void removerItems(List<ReservaIG.Modelo.Entidades.Menu> menus, CheckedListBox checkedListBox)
        {
            if(checkedListBox.Items.Count <= 0)
            {
                return;
            }
            foreach (var item in menus)
            {
                for(int i=0;  i<checkedListBox.Items.Count; i++)
                {
                    if (checkedListBox.Items[i].ToString() == item.Nombre)
                    {
                        checkedListBox.Items.RemoveAt(i);
                    }
                }
            }
        }

        private void removerItems(ReservaIG.Modelo.Entidades.Menu menu, CheckedListBox checkedListBox)
        {
            if (checkedListBox.Items.Count <= 0)
            {
                return;
            }
            for (int i = 0; i < checkedListBox.Items.Count; i++)
            {
                if (checkedListBox.Items[i].ToString() == menu.Nombre)
                {
                    checkedListBox.Items.RemoveAt(i);
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
    }
}
