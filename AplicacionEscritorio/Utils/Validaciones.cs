﻿using ReservaIG.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AplicacionEscritorio.Utils
{
    class Validaciones
    {
        /// <summary>
        /// Validar si el password cumple con la politica de:
        /// -Minimo 8 caracteres
        /// -Mínimo 1 Mayuscula
        /// -Minimo 1 Minuscula
        /// -Minimo 1 caracter especial
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        internal static bool ValidarPassword(string password)
        {
            Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$");
            return regex.IsMatch(password);
        }

        /// <summary>
        /// Valida si un nombre de usuario existe o no en la base de datos.
        /// Si existe retorna falso, sino retorna verdadero
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        internal static bool ValidarUsername(string username)
        {
            return new ModeloFachada().ValidarNombreUsuario(username);
        }

    }
}
