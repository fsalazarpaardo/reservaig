﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AplicacionEscritorio.Utils
{
    internal static class SessionHelper
    {
        internal static string username { get; set; }
        internal static void ClearSpace(Control control)
        {
            foreach (Control c in control.Controls)
            {
                var textBox = c as TextBox;
                var comboBox = c as ComboBox;
                var radioButton = c as RadioButton;

                if (textBox != null)
                    (textBox).Clear();

                if (comboBox != null)
                    comboBox.SelectedIndex = -1;

                if (radioButton != null)
                    radioButton.Checked = false;

                if (c.HasChildren)
                    ClearSpace(c);
            }
        }

    }
}
